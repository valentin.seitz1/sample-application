# My awesome application

This repo showcases how applications can integrate the DLB TALP components in their CI.
Therefore we "emulate" the instrumented run on the cluster which produces the `talp.json` with a simple: `python generate_talp_json.py` invocation.
