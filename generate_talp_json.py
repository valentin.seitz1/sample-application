
base_case="""
{
  "popMetrics": [
    {
      "name": "MPI Execution",
      "elapsedTime": 192415381554,
      "averageIPC": 1.40,
      "parallelEfficiency": 0.5,
      "communicationEfficiency": 0.98,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "add",
      "elapsedTime": 1055310913,
      "averageIPC": 0.20,
      "parallelEfficiency": 1.00,
      "communicationEfficiency": 1.00,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "calc_c",
      "elapsedTime": 15340870944,
      "averageIPC": 2.64,
      "parallelEfficiency": 0.98,
      "communicationEfficiency": 0.98,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "calc_a",
      "elapsedTime": 1568096983,
      "averageIPC": 0.43,
      "parallelEfficiency": 1.00,
      "communicationEfficiency": 1.00,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "calc_b",
      "elapsedTime": 81306824886,
      "averageIPC": 2.43,
      "parallelEfficiency": 1.00,
      "communicationEfficiency": 1.00,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "copy",
      "elapsedTime": 196910290,
      "averageIPC": 0.29,
      "parallelEfficiency": 1.00,
      "communicationEfficiency": 1.00,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "exchange",
      "elapsedTime": 6301841529,
      "averageIPC": 2.92,
      "parallelEfficiency": 0.47,
      "communicationEfficiency": 0.47,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "file_io",
      "elapsedTime": 3412229838,
      "averageIPC": 1.39,
      "parallelEfficiency": 0.97,
      "communicationEfficiency": 0.97,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "app",
      "elapsedTime": 192180988291,
      "averageIPC": 2.40,
      "parallelEfficiency": 0.98,
      "communicationEfficiency": 0.98,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "initialize",
      "elapsedTime": 41094576131,
      "averageIPC": 2.92,
      "parallelEfficiency": 1.00,
      "communicationEfficiency": 1.00,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "initialize_mesh",
      "elapsedTime": 27234019115,
      "averageIPC": 3.23,
      "parallelEfficiency": 1.00,
      "communicationEfficiency": 1.00,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "initialize_timestep",
      "elapsedTime": 13860395248,
      "averageIPC": 2.35,
      "parallelEfficiency": 1.00,
      "communicationEfficiency": 1.00,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "lin_comb",
      "elapsedTime": 827419076,
      "averageIPC": 0.39,
      "parallelEfficiency": 1.00,
      "communicationEfficiency": 1.00,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "mpi_allreduce",
      "elapsedTime": 813955761,
      "averageIPC": 0.14,
      "parallelEfficiency": 0.00,
      "communicationEfficiency": 0.00,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "calc_d",
      "elapsedTime": 14298092539,
      "averageIPC": 2.71,
      "parallelEfficiency": 1.00,
      "communicationEfficiency": 1.00,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "calc_e",
      "elapsedTime": 1042631545,
      "averageIPC": 1.20,
      "parallelEfficiency": 0.66,
      "communicationEfficiency": 0.66,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "calc_f",
      "elapsedTime": 817950424,
      "averageIPC": 1.73,
      "parallelEfficiency": 0.77,
      "communicationEfficiency": 0.77,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "calc_g",
      "elapsedTime": 2039919481,
      "averageIPC": 0.90,
      "parallelEfficiency": 0.91,
      "communicationEfficiency": 0.91,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "calc_h",
      "elapsedTime": 1567973599,
      "averageIPC": 0.43,
      "parallelEfficiency": 1.00,
      "communicationEfficiency": 1.00,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "calc_i",
      "elapsedTime": 81306536383,
      "averageIPC": 2.43,
      "parallelEfficiency": 1.00,
      "communicationEfficiency": 1.00,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "calc_j",
      "elapsedTime": 14012479929,
      "averageIPC": 1.99,
      "parallelEfficiency": 1.00,
      "communicationEfficiency": 1.00,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "calc_k",
      "elapsedTime": 6180503807,
      "averageIPC": 2.49,
      "parallelEfficiency": 1.00,
      "communicationEfficiency": 1.00,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    },
    {
      "name": "calc_l",
      "elapsedTime": 17007197976,
      "averageIPC": 1.89,
      "parallelEfficiency": 1.00,
      "communicationEfficiency": 1.00,
      "loadBalance": 1.00,
      "lbIn": 1.00,
      "lbOut": 1.00
    }
  ]
}"""
import random 
import json

to_modify=['elapsedTime','averageIPC','parallelEfficiency']
data = json.loads(base_case)
for entry in data['popMetrics']:
    for mod in to_modify:
        factor = random.uniform(0.75, 1.45)
        entry[mod]*=factor
        if(mod=='elapsedTime'):
            entry[mod]=int(entry[mod])

with open('talp.json', 'w') as f:
    json.dump(data, f)
